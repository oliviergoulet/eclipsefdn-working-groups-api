/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.services;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.wg.api.SysAPI;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service interface to retrieve system data from the foundation DB service.
 * 
 * @author Martin Lowe
 *
 */
public interface SystemDataService {

    /**
     * Get the relation data from the system data endpoint for foundationdb. Retrieves only data related to the WG
     * relations needed by this service.
     * 
     * @return list of sys relation data for working group type relations.
     */
    List<SysRelationData> get();

    @ApplicationScoped
    public static class DefaultSystemDataService implements SystemDataService {
        public static final Logger LOGGER = LoggerFactory.getLogger(DefaultSystemDataService.class);

        public static final String WORKING_GROUPS_RELATION_TYPE = "WG";

        @Inject
        CachingService cache;
        @Inject
        APIMiddleware middle;

        @RestClient
        @Inject
        SysAPI sysAPI;

        @Override
        public List<SysRelationData> get() {
            Optional<List<SysRelationData>> relations = cache.get(WORKING_GROUPS_RELATION_TYPE,
                    new MultivaluedMapImpl<>(), SysRelationData.class,
                    () -> middle.getAll(i -> sysAPI.get(i, WORKING_GROUPS_RELATION_TYPE), SysRelationData.class));
            if (relations.isEmpty()) {
                LOGGER.warn("Could not find any relations for type '{}', there may be a connection error",
                        WORKING_GROUPS_RELATION_TYPE);
                return Collections.emptyList();
            }
            return relations.get();
        }
    }
}
