/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipsefoundation.wg.models.WorkingGroupMap;

/**
 * Defines a service that will return available working group data, allowing for retrieval by alias name, or the full
 * set.
 * 
 * @author Martin Lowe, Zachary Sabourin
 *
 */
public interface WorkingGroupsService {

    /**
     * Returns a set of working groups, with an optional filter of the project status.
     * 
     * @param projectStatus optional, statuses to include in result set
     * @return set of working groups matching optional filters if present, otherwise all available working groups
     */
    public Set<WorkingGroupMap.WorkingGroup> get(List<String> projectStatus);

    public WorkingGroupMap.WorkingGroup getByAlias(String alias);

    public Map<String, List<String>> getWGPADocumentIDs();
}
