/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.wg.models.WorkingGroupMap.WorkingGroup;
import org.eclipsefoundation.wg.services.WorkingGroupsService;

/**
 * Retrieves working group definitions from the working groups service.
 *
 * @author Martin Lowe, Zachary Sabourin
 */
@Path("working-groups")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WorkingGroupsResource {

    @Inject
    WorkingGroupsService wgService;

    @GET
    public Response getWorkingGroups(@QueryParam("status") List<String> statuses) {
        // return the results as a response
        return Response.ok(new ArrayList<>(wgService.get(statuses))).build();
    }

    @GET
    @Path("{alias}")
    public Response getWorkingGroup(@PathParam("alias") String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            return Response.status(404).build();
        }

        return Response.ok(wg).build();
    }

    @GET
    @Path("{alias}/resources")
    public Response getWorkingGroupResources(@PathParam("alias") String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            return Response.status(404).build();
        }

        return Response.ok(wg.getResources()).build();
    }

    @GET
    @Path("{alias}/levels")
    public Response getWorkingGroupLevels(@PathParam("alias") String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            return Response.status(404).build();
        }

        return Response.ok(wg.getLevels()).build();
    }

    @GET
    @Path("{alias}/agreements")
    public Response getWorkingGroupAgreements(@PathParam("alias") String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            return Response.status(404).build();
        }

        return Response.ok(wg.getResources().getParticipationAgreements()).build();
    }
}
