SHELL = /bin/bash

pre-setup:;
	@echo "Creating environment file from template"
	@rm -f .env && envsubst < config/.env.sample > .env

setup:;
	@echo "Generating secret files from templates using environment file + variables"
	@source .env && rm -f ./config/secret.properties && envsubst < config/secret.properties.sample > config/secret.properties
	@source .env && rm -f ./config/foundationdb/secret.properties && envsubst < config/foundationdb/secret.properties.sample > config/foundationdb/secret.properties

dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties

clean:;
	mvn clean

install-yarn:;
	yarn install --frozen-lockfile --audit

generate-spec: install-yarn validate-spec;
	yarn run generate-json-schema

validate-spec: install-yarn;

compile-java: generate-spec;
	mvn compile package

compile-java-quick: generate-spec;
	mvn compile package -Dmaven.test.skip=true

compile: clean compile-java;

compile-quick: clean compile-java-quick;
