# eclipsefdn-working-group-api Project

This project uses Quarkus, the Supersonic Subatomic Java Framework. If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Setting up environment

Before running the application, the initial setup should be observed. The initial setup of this server can be started by running the command `make setup` which will instantiate the secrets file and environment hidden file. To finish the setup of the environment file, open it and add values to the missing fields before starting docker for this project. The missing fields are primarily passwords for the various services running in docker for the project. Set them to some values and save the file. Once finished you can run the docker-compose command for this project successfully. If you need to start MariaDB (not using a shared volume) you can use `docker-compose --profile full up` to start MariaDB along with other services.

Additionally, some of the values in the .env file will need to be set into the secret.properties files in `./config/secret.properties` and `./config/foundationdb/secret.properties`. The value for `WGAPI_MYSQL_PASSWORD` will need to be set as the value for the `quarkus.datasource.password` property in both files. This will give the applications access to make changes to the databases.

Once the stack is running, navigate to http://localhost:8080 and log into Keycloak using the credentials set in the `.env` file. Upon logging in in the top left corner, if the text says `Master`, then hover over the text and select `Foundation` in the pop up that appears below the text. This switches your realm to the one configured at boot time by the embedded configuration files. In the left hand menu, select `Clients` and open the `foundation` client by clicking on the name of the client. Once here, navigate to the `Credentials` tab and regenerate the secret. That text will be the value of the `quarkus.oidc.credentials.client-secret.value` property in the `./config/foundationdb/secret.properties` file. Do the same thing for the `working_groups_api` client, instead setting the value of `quarkus.oidc.credentials.secret` property in the `./config/secret.properties` file.

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
mvn compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
mvn package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
mvn package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
mvn package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
mvn package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/eclipsefdn-working-group-api-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- RESTEasy Classic ([guide](https://quarkus.io/guides/resteasy)): REST endpoint framework implementing JAX-RS and more

## Provided Code

### RESTEasy JAX-RS

Easily start your RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started#the-jax-rs-resources)
